# Custom component for Snapcast integration: add stream state sensor
This custom component adds a sensor for every stream in the snapcast server. The state of te stream is shown in the sensor. This can be playing, idle or unavailable. When the stream changes state, the state is pushed into home assistant with a callback.

Just put these files in the custom_components folder of your config directory. Because the naming of the component is the same as the existing one in the components, it will override it.

## Extra optional option: hide groups
I do not use the group functionality in Home assistant. So, I made an optional property to not instantiate a group in home assistant. This is optional. See example below

## Warning: breaking change

When you use this component, there will be a breaking change. Instead of instantiating it in the media_player component, it is now directly in the configuration.

configuration.yaml example:

```
snapcast:
  host: "192.168.1.100" # snapcast server ID
  port: 1705 # optional
  hide_groups: False # optional. I do not use groups
```

