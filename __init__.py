"""The snapcast component."""
import logging
import socket


import voluptuous as vol

import snapcast.control
from snapcast.control.server import CONTROL_PORT

from homeassistant.helpers.discovery import async_load_platform

from homeassistant.helpers import config_validation as cv, entity_platform

from homeassistant.const import (
    CONF_HOST,
    CONF_PORT,
)

from .const import (
    ATTR_LATENCY,
    ATTR_MASTER,
    CLIENT_PREFIX,
    CLIENT_SUFFIX,
    DATA_KEY,
    GROUP_PREFIX,
    GROUP_SUFFIX,
    SERVICE_JOIN,
    SERVICE_RESTORE,
    SERVICE_SET_LATENCY,
    SERVICE_SNAPSHOT,
    SERVICE_UNJOIN,
    GROUP_DISABLE,
)

_LOGGER = logging.getLogger(__name__)

# PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend(
#   {vol.Required(CONF_HOST): cv.string, vol.Optional(CONF_PORT): cv.port}
# )

CONFIG_SCHEMA = vol.Schema(
    {
        DATA_KEY: vol.Schema(
            {
                vol.Required(CONF_HOST): cv.string,
                vol.Optional(CONF_PORT): cv.port,
                vol.Optional(GROUP_DISABLE): cv.boolean,
            }
        )
    },
    extra=vol.ALLOW_EXTRA,
)


async def async_setup(hass, config):
    """setup of platform"""
    conf = config.get(DATA_KEY)
    host = conf.get(CONF_HOST)
    port = conf.get(CONF_PORT, CONTROL_PORT)

    try:
        server = await snapcast.control.create_server(hass.loop, host, port, reconnect=True)
    except socket.gaierror:
        _LOGGER.error("Could not connect to Snapcast server at %s:%d", host, port)
        return

    # Note: Host part is needed, when using multiple snapservers
    hpid = f"{host}:{port}"

    hass.data[DATA_KEY] = {
        "server": server,
        "hpid": hpid,
        GROUP_DISABLE: conf.get(GROUP_DISABLE, False),
    }

    hass.async_create_task(
        async_load_platform(hass, "media_player", DATA_KEY, {}, config)
    )

    hass.async_create_task(async_load_platform(hass, "sensor", DATA_KEY, {}, config))

    return True


# SNAPSERVER OVERRIDE CLASS. Not needed anymore when python snapcast is updated. See: https://github.com/happyleavesaoc/python-snapcast/pull/35

#import asyncio
#from snapcast.control.stream import Snapstream
#from snapcast.control.server import Snapserver
#from snapcast.control.client import Snapclient
#from snapcast.control.group import Snapgroup
#
#
#@asyncio.coroutine
#def create_server(loop, host, port=CONTROL_PORT, reconnect=False):
#    """Server factory."""
#    server = CustomSnapserver(loop, host, port, reconnect)
#    yield from server.start()
#    return server
#
#
#class CustomSnapstream(Snapstream):
#    def __init__(self, data):
#        """Initialize."""
#        self.update(data)
#        self._callback_func = None
#
#    def callback(self):
#        """Run callback."""
#        if self._callback_func and callable(self._callback_func):
#            self._callback_func(self)
#
#    def set_callback(self, func):
#        """Set callback."""
#        self._callback_func = func
#
#
#class CustomSnapserver(Snapserver):
#    """override snapserver"""
#
#    def _on_stream_update(self, data):
#        """Handle stream update."""
#        self._streams[data.get("id")].update(data.get("stream"))
#        _LOGGER.info(
#            "custom stream %s updated", self._streams[data.get("id")].friendly_name
#        )
#        self._streams[data.get("id")].callback()
#        for group in self._groups.values():
#            if group.stream == data.get("id"):
#                group.callback()
#
#    def synchronize(self, status):
#        """Synchronize snapserver."""
#        self._version = status.get("server").get("version")
#        self._groups = {}
#        self._clients = {}
#        self._streams = {}
#        for stream in status.get("server").get("streams"):
#            self._streams[stream.get("id")] = CustomSnapstream(stream)
#            _LOGGER.debug("stream found: %s", self._streams[stream.get("id")])
#        for group in status.get("server").get("groups"):
#            self._groups[group.get("id")] = Snapgroup(self, group)
#            _LOGGER.debug("group found: %s", self._groups[group.get("id")])
#            for client in group.get("clients"):
#                self._clients[client.get("id")] = Snapclient(self, client)
#                _LOGGER.debug("client found: %s", self._clients[client.get("id")])
